import React from 'react'
import axios from 'axios'
export default class extends React.Component {
  state = {
    data: null,
    a: 1
  }
  static async getInitialProps({ req }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
    return { userAgent }
  }

  componentDidMount(){
    const self = this
      axios({
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:3030/api/v1/menus?page=1&size=100",
        "method": "GET"
      }).then(r=>{
        self.setState({
          data: r.data
        })
      }).catch(err=> {
        console.log(err)
      })
  }

  render() {
    console.log(this.state.data)
    return (
      <div>
        Hello World {this.state.a}
      </div>
    )
  }
}