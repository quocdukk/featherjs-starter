import { Product } from "../../models"
import { NotFound, Forbidden, BadRequest } from '@feathersjs/errors'
import Joi from 'joi'
import { CategoryController } from "../category/CategoryController";

class ProductController extends CategoryController{
    constructor(){
        super()
    }

    async find(params){

    }

    async get(id, params){

    }

    async create(data,params){
        
    }

    async update(id, data,params){
        
    }

    async delete(id, data, params){
        
    }
}

export { ProductController }