import { Category, Menu } from "../../models"
import { NotFound, Forbidden, BadRequest } from '@feathersjs/errors'
import Joi from 'joi'
import Controller from '../../extends/Controller'
class CategoryController extends Controller{

    async find(params){
        const { page, size } = params.query
        return await Category.paginate({
            page: page >= 1 ? page : 1, // Default 1
            paginate: size >= 1 ? size : 1, // Default 25
            where: { active_flag: true },
            include: [{
                model : Menu
            }]
        })
    }

    async get(id, params){

    }

    async create(data,params){
        
    }

    async update(id, data,params){
        
    }

    async delete(id, data, params){
        
    }
}

export { CategoryController }