import { NotFound, Forbidden, BadRequest } from '@feathersjs/errors'
import { Menu, Category } from '../../models';
import { menuSchema } from '../../validate';
import Joi from 'joi'
class MenuController{

    constructor(){
    }

    async find(params){
        const { page, size } = params.query
        return await Menu.paginate({
            page: page >= 1 ? page : 1, // Default 1
            paginate: size >= 1 ? size : 1, // Default 25
            where: { active_flag: true },
            include: [{
                model : Category
            }] 
        })
    }

    async create(data, params){
        const {error, value} = Joi.validate({
            name: data.name,
            description: data.description
        }, menuSchema)
        if(error) return await new BadRequest(error)
        return await Menu.create({
            name: value.name,description:value.description
        })
    }

    async get(id, params){
        return await Menu.findOne({
            where: { id,active_flag: true }
        })
    }

    async update(id, data,params){
        const {error, value} = Joi.validate({
            name: data.name,
            description: data.description,
            active_flag: data.active_flag
        }, menuSchema)
        if(error) return await new BadRequest(error)
        return await Menu.update({
            name: value.name,description: value.description, active_flag: value.active_flag
        },{
            where: {id},
            benchmark: true
        })
    }

    async remove(id, data,params){
        return await Menu.update({
            active_flag : false
        },{
            where: {id},
            benchmark: true
        })
    }
}

export { MenuController }