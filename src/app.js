import feather from '@feathersjs/feathers'
import express from '@feathersjs/express'
import morgan from 'morgan'
import logger from 'feathers-logger'
import socketio from '@feathersjs/socketio'
// import service from 'feathers-sequelize'
import cors from 'cors'
import compression from 'compression'
import { MenuController, CategoryController } from './controllers'
import { sequelize } from './config/db.config';
// form data multi part
import { formdata } from './config/form.config'
import {Menu,Product, Category, TypeProduct, Review,Comment,Promotion,Specification,Type} from './models'
const app = express(feather())

// enable cors
// Enable CORS
app.use(compression())
app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.configure(logger(morgan({
    format: 'dev'
}))).configure(express.rest())

// enable socketio
app.configure(socketio());

// test connect
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

app.use('/api/v1/menus', new MenuController())
app.use('/api/v1/categories', new CategoryController())


app.on('connection', connection => app.channel('everybody').join(connection))
app.publish(() => app.channel('everybody'));
app.use(express.errorHandler());
// Start the server on port 3030
const server = app.listen(3030 || process.env.PORT);

server.on('listening', () => console.log('Feathers API started at localhost:3030'));