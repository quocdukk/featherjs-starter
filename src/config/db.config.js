import Sequelize from 'sequelize';
// import service from 'feathers-sequelize';

const sequelize = new Sequelize('tgdd', '', '', {
  dialect: 'sqlite',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  // SQLite only
  storage: 'db/database.sqlite3',
});

export { sequelize, Sequelize  }