import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export const encode_text = async (text) => {
    const hash = await bcrypt.hashSync(text, 10)
    return hash
}

export const compare_hash = async (text, hash) => {
    const verify = await bcrypt.compareSync(text, hash)
    return verify
}

export const verify_token = async (token, secret_key) => {
    const verify = await jwt.verify(token, secret_key)
    return verify
}

export const encode_jwt = (data, secret_key) => {
    return new Promise((reject, resolve)=>{
        jwt.sign(data, secret_key,{expiresIn:'1d'}, (err, token)=>{
            if(err) reject(err)
            resolve({
                token
            })
        })
    })
}

export const verifyToken = (req, res, next) => {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
      // Split at the space
      const bearer = bearerHeader.split(' ');
      // Get token from array
      const bearerToken = bearer[1];
      // Set the token
      req.feathers.token = bearerToken;
      // Next middleware
      next();
    } else {
      // Forbidden
      res.sendStatus(403);
    }
}