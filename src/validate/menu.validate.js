import Joi  from 'joi'

const menuSchema = Joi.object().keys({
    name: Joi.string().required().regex(/^[a-zA-Z]/),
    description: Joi.string(),
    active_flag: Joi.boolean().default(true)
})

export { menuSchema }