import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Category } from './Category';
import { Product } from './Product';

export const Tag = sequelize.define('tags', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    category_id: {
        type: Sequelize.INTEGER,
        references: {
            key: 'id',
            model: Category
        }
    },
    product_id: {
        type: Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: Product
        }
    }
})

Tag.sync({
    force: true
}).then(() => {
    // Table created
    
});