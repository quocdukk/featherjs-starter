import {
    sequelize,
    Sequelize
} from '../config/db.config'
import {paginate} from 'sequelize-paginate'
export const Menu = sequelize.define('menu', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        // unique: true
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
            fields: ['name'],
            unique: true
        },
        {
            fields: ['id']
        }
    ]
})
paginate(Menu)
Menu.sync({
    force: true
}).then(() => {
    // Table created
    // let data = [{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // }]
    // return Menu.bulkCreate(data)
});