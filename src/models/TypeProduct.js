import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Product } from './Product';
import { Type } from './Type';

export const TypeProduct = sequelize.define('type_products', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: Product
        }
    },
    type_id: {
        type: Sequelize.INTEGER,
        references: {
            key: 'id',
            model: Type
        }
    }
})

TypeProduct.sync({
    force: true
}).then(() => {
    // Table created
    
});