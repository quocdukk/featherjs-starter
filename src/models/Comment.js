import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Review } from './Review';

export const Comment = sequelize.define('comments', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    review_id: {
        type: Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: Review
        },
        allowNull: true
    },
    user_ref: {
        type: Sequelize.STRING({length:50})
    },
    name: {
        type: Sequelize.STRING({length:50}),
        allowNull: true
    },
    image: {
        type: Sequelize.STRING,
        allowNull: true
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
})

Comment.sync({
    force: true
}).then(() => {
    // Table created
    
});