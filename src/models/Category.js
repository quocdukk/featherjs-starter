import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Menu } from './Menu';
import {paginate} from 'sequelize-paginate'
export const Category = sequelize.define('category', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
            fields: ['name'],
            unique: true
        },
        {
            fields: ['id']
        }
    ]
})
Category.belongsTo(Menu)
paginate(Category)
Category.sync({
    force: true
}).then(() => {
    // Table created
    // let data = [{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    //     menuId:1
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    //     menuId:1
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // }]
    // return Category.bulkCreate(data)
});