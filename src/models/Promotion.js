import {
    sequelize,
    Sequelize
} from '../config/db.config'

export const Promotion = sequelize.define('promotions', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    title_image: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    start_date: {
        type: Sequelize.DATE,
        defaultValue: Date.now()
    },
    percentage: {
        type: Sequelize.INTEGER({length:100})
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [
        {
            fields: ['id', 'title'],
            unique: true
        },
        {
            fields: ['start_date']
        }
    ]
})

Promotion.sync({
    force: true
}).then(()=>{

})