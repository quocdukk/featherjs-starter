import { Product } from './Product'
import { Menu } from './Menu'
import { Category } from './Category'
import { Review } from './Review'
import { Comment } from './Comment'
import { Type } from './Type'
import { TypeProduct } from './TypeProduct'
import { Specification } from './Specification'
import { Promotion } from './Promotion'
import { Tag } from './Tag'

Menu.hasMany(Category)

export {
    Product,
    Menu,
    Category,
    Review,
    Comment,
    Type,
    TypeProduct,
    Specification,
    Promotion,
    Tag
}