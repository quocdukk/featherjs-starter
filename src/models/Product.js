import {
    sequelize,
    Sequelize
} from '../config/db.config'

export const Product = sequelize.define('products', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    sort_description: {
        type: Sequelize.STRING({
            length: 200
        }),
        allowNull: true
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    stock: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    price: {
        type: Sequelize.DECIMAL(10, 2),
        defaultValue: 0
    },
    vat: {
        type: Sequelize.DECIMAL(10, 2),
        defaultValue: 0
    },
    status: {
        type: Sequelize.ENUM({
            values: ['pending', 'ready', 'expire']
        }),
        defaultValue: 'pending'
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
},{
    indexes : [
        {
            fields: ['title'],
            unique: true
        },
        {
            fields: ['price']
        }
    ]
})

Product.sync({
    force: true
}).then(() => {
    // Table created
    
});