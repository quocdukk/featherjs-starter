import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Product } from './Product';

export const Review = sequelize.define('reviews', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: Product
        }
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    fullname: {
        type: Sequelize.STRING,
        allowNull: true
    },
    phone: {
        type: Sequelize.STRING({length: 10}),
        allowNull: true
    },
    review: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    value: {
        type: Sequelize.INTEGER({length:5}),
       allowNull: false
    }
},{
    indexes: [
        {
            fields: ['id', 'email'],
            unique: true
        }
    ]
})

Review.sync({
    force: true
}).then(() => {
    // Table created
    
});