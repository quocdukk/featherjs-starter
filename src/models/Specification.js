import {
    sequelize,
    Sequelize
} from '../config/db.config'
import { Product } from './Product'

export const Specification = sequelize.define('specifications', {
    id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: Product
        }
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: true
    }
})

Specification.sync({
    force: true
}).then(()=>{

})