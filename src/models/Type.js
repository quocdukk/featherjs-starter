import {
    sequelize,
    Sequelize
} from '../config/db.config'

export const Type = sequelize.define('types', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING({length:100}),
        allowNull: false
    },
    active_flag: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
})

Type.sync({
    force: true
}).then(() => {
    // Table created
    
});