"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var key_admin = exports.key_admin = "admin";
var key_customer = exports.key_customer = "customer";

var verifyToken = exports.verifyToken = function verifyToken(req, res, next) {
  // Get auth header value
  var bearerHeader = req.headers['authorization'];
  // Check if bearer is undefined
  if (typeof bearerHeader !== 'undefined') {
    // Split at the space
    var bearer = bearerHeader.split(' ');
    // Get token from array
    var bearerToken = bearer[1];
    // Set the token
    req.feathers.token = bearerToken;
    // Next middleware
    next();
  } else {
    // Forbidden
    res.sendStatus(403);
  }
};