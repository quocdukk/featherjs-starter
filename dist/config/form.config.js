'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formdata = undefined;

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formdata = exports.formdata = (0, _multer2.default)().fields([]);