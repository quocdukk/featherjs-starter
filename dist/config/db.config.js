'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Sequelize = exports.sequelize = undefined;

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import service from 'feathers-sequelize';

var sequelize = new _sequelize2.default('tgdd', '', '', {
  dialect: 'sqlite',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  // SQLite only
  storage: 'db/database.sqlite3'
});

exports.sequelize = sequelize;
exports.Sequelize = _sequelize2.default;