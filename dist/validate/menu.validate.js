'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.menuSchema = undefined;

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var menuSchema = _joi2.default.object().keys({
    name: _joi2.default.string().required().regex(/^[a-zA-Z]/),
    description: _joi2.default.string(),
    active_flag: _joi2.default.boolean().default(true)
});

exports.menuSchema = menuSchema;