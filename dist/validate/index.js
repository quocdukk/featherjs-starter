'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _menu = require('./menu.validate');

Object.defineProperty(exports, 'menuSchema', {
  enumerable: true,
  get: function get() {
    return _menu.menuSchema;
  }
});