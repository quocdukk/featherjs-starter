'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.verifyToken = exports.encode_jwt = exports.verify_token = exports.compare_hash = exports.encode_text = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var encode_text = exports.encode_text = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(text) {
        var hash;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return _bcrypt2.default.hashSync(text, 10);

                    case 2:
                        hash = _context.sent;
                        return _context.abrupt('return', hash);

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function encode_text(_x) {
        return _ref.apply(this, arguments);
    };
}();

var compare_hash = exports.compare_hash = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee2(text, hash) {
        var verify;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.next = 2;
                        return _bcrypt2.default.compareSync(text, hash);

                    case 2:
                        verify = _context2.sent;
                        return _context2.abrupt('return', verify);

                    case 4:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function compare_hash(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

var verify_token = exports.verify_token = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee3(token, secret_key) {
        var verify;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.next = 2;
                        return _jsonwebtoken2.default.verify(token, secret_key);

                    case 2:
                        verify = _context3.sent;
                        return _context3.abrupt('return', verify);

                    case 4:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, undefined);
    }));

    return function verify_token(_x4, _x5) {
        return _ref3.apply(this, arguments);
    };
}();

var encode_jwt = exports.encode_jwt = function encode_jwt(data, secret_key) {
    return new Promise(function (reject, resolve) {
        _jsonwebtoken2.default.sign(data, secret_key, { expiresIn: '1d' }, function (err, token) {
            if (err) reject(err);
            resolve({
                token: token
            });
        });
    });
};

var verifyToken = exports.verifyToken = function verifyToken(req, res, next) {
    // Get auth header value
    var bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        var bearer = bearerHeader.split(' ');
        // Get token from array
        var bearerToken = bearer[1];
        // Set the token
        req.feathers.token = bearerToken;
        // Next middleware
        next();
    } else {
        // Forbidden
        res.sendStatus(403);
    }
};