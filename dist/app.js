'use strict';

var _feathers = require('@feathersjs/feathers');

var _feathers2 = _interopRequireDefault(_feathers);

var _express = require('@feathersjs/express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _feathersLogger = require('feathers-logger');

var _feathersLogger2 = _interopRequireDefault(_feathersLogger);

var _socketio = require('@feathersjs/socketio');

var _socketio2 = _interopRequireDefault(_socketio);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _controllers = require('./controllers');

var _db = require('./config/db.config');

var _form = require('./config/form.config');

var _models = require('./models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// form data multi part

// import service from 'feathers-sequelize'
var app = (0, _express2.default)((0, _feathers2.default)());

// enable cors
// Enable CORS
app.use((0, _compression2.default)());
app.use((0, _cors2.default)());
app.use(_express2.default.json());
app.use(_express2.default.urlencoded({ extended: true }));
app.configure((0, _feathersLogger2.default)((0, _morgan2.default)({
    format: 'dev'
}))).configure(_express2.default.rest());

// enable socketio
app.configure((0, _socketio2.default)());

// test connect
_db.sequelize.authenticate().then(function () {
    console.log('Connection has been established successfully.');
}).catch(function (err) {
    console.error('Unable to connect to the database:', err);
});

app.use('/api/v1/menus', new _controllers.MenuController());
app.use('/api/v1/categories', new _controllers.CategoryController());

app.on('connection', function (connection) {
    return app.channel('everybody').join(connection);
});
app.publish(function () {
    return app.channel('everybody');
});
app.use(_express2.default.errorHandler());
// Start the server on port 3030
var server = app.listen(3030 || process.env.PORT);

server.on('listening', function () {
    return console.log('Feathers API started at localhost:3030');
});