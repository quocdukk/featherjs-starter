'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _MenuController = require('./menu/MenuController');

Object.defineProperty(exports, 'MenuController', {
  enumerable: true,
  get: function get() {
    return _MenuController.MenuController;
  }
});

var _CategoryController = require('./category/CategoryController');

Object.defineProperty(exports, 'CategoryController', {
  enumerable: true,
  get: function get() {
    return _CategoryController.CategoryController;
  }
});