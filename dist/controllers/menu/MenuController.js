'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MenuController = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _errors = require('@feathersjs/errors');

var _models = require('../../models');

var _validate = require('../../validate');

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MenuController = function () {
    function MenuController() {
        _classCallCheck(this, MenuController);
    }

    _createClass(MenuController, [{
        key: 'find',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(params) {
                var _params$query, page, size;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _params$query = params.query, page = _params$query.page, size = _params$query.size;
                                _context.next = 3;
                                return _models.Menu.paginate({
                                    page: page >= 1 ? page : 1, // Default 1
                                    paginate: size >= 1 ? size : 1, // Default 25
                                    where: { active_flag: true },
                                    include: [{
                                        model: _models.Category
                                    }]
                                });

                            case 3:
                                return _context.abrupt('return', _context.sent);

                            case 4:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function find(_x) {
                return _ref.apply(this, arguments);
            }

            return find;
        }()
    }, {
        key: 'create',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee2(data, params) {
                var _Joi$validate, error, value;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _Joi$validate = _joi2.default.validate({
                                    name: data.name,
                                    description: data.description
                                }, _validate.menuSchema), error = _Joi$validate.error, value = _Joi$validate.value;

                                if (!error) {
                                    _context2.next = 5;
                                    break;
                                }

                                _context2.next = 4;
                                return new _errors.BadRequest(error);

                            case 4:
                                return _context2.abrupt('return', _context2.sent);

                            case 5:
                                _context2.next = 7;
                                return _models.Menu.create({
                                    name: value.name, description: value.description
                                });

                            case 7:
                                return _context2.abrupt('return', _context2.sent);

                            case 8:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function create(_x2, _x3) {
                return _ref2.apply(this, arguments);
            }

            return create;
        }()
    }, {
        key: 'get',
        value: function () {
            var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee3(id, params) {
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return _models.Menu.findOne({
                                    where: { id: id, active_flag: true }
                                });

                            case 2:
                                return _context3.abrupt('return', _context3.sent);

                            case 3:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function get(_x4, _x5) {
                return _ref3.apply(this, arguments);
            }

            return get;
        }()
    }, {
        key: 'update',
        value: function () {
            var _ref4 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee4(id, data, params) {
                var _Joi$validate2, error, value;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _Joi$validate2 = _joi2.default.validate({
                                    name: data.name,
                                    description: data.description,
                                    active_flag: data.active_flag
                                }, _validate.menuSchema), error = _Joi$validate2.error, value = _Joi$validate2.value;

                                if (!error) {
                                    _context4.next = 5;
                                    break;
                                }

                                _context4.next = 4;
                                return new _errors.BadRequest(error);

                            case 4:
                                return _context4.abrupt('return', _context4.sent);

                            case 5:
                                _context4.next = 7;
                                return _models.Menu.update({
                                    name: value.name, description: value.description, active_flag: value.active_flag
                                }, {
                                    where: { id: id },
                                    benchmark: true
                                });

                            case 7:
                                return _context4.abrupt('return', _context4.sent);

                            case 8:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function update(_x6, _x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return update;
        }()
    }, {
        key: 'remove',
        value: function () {
            var _ref5 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee5(id, data, params) {
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.next = 2;
                                return _models.Menu.update({
                                    active_flag: false
                                }, {
                                    where: { id: id },
                                    benchmark: true
                                });

                            case 2:
                                return _context5.abrupt('return', _context5.sent);

                            case 3:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));

            function remove(_x9, _x10, _x11) {
                return _ref5.apply(this, arguments);
            }

            return remove;
        }()
    }]);

    return MenuController;
}();

exports.MenuController = MenuController;