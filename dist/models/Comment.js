'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Comment = undefined;

var _db = require('../config/db.config');

var _Review = require('./Review');

var Comment = exports.Comment = _db.sequelize.define('comments', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    review_id: {
        type: _db.Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: _Review.Review
        },
        allowNull: true
    },
    user_ref: {
        type: _db.Sequelize.STRING({ length: 50 })
    },
    name: {
        type: _db.Sequelize.STRING({ length: 50 }),
        allowNull: true
    },
    image: {
        type: _db.Sequelize.STRING,
        allowNull: true
    },
    content: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
});

Comment.sync({
    force: true
}).then(function () {
    // Table created

});