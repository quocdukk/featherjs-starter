'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Promotion = undefined;

var _db = require('../config/db.config');

var Promotion = exports.Promotion = _db.sequelize.define('promotions', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    title: {
        type: _db.Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    title_image: {
        type: _db.Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: _db.Sequelize.TEXT,
        allowNull: false
    },
    start_date: {
        type: _db.Sequelize.DATE,
        defaultValue: Date.now()
    },
    percentage: {
        type: _db.Sequelize.INTEGER({ length: 100 })
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
        fields: ['id', 'title'],
        unique: true
    }, {
        fields: ['start_date']
    }]
});

Promotion.sync({
    force: true
}).then(function () {});