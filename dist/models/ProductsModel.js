'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Products = undefined;

var _db = require('../config/db.config');

var Products = exports.Products = _db.sequelize.define('products', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true
    },
    title: {
        type: _db.Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    sort_description: {
        type: _db.Sequelize.STRING({
            length: 200
        }),
        allowNull: true
    },
    description: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    },
    stock: {
        type: _db.Sequelize.INTEGER,
        defaultValue: 0
    },
    price: {
        type: _db.Sequelize.DECIMAL(10, 2),
        defaultValue: 0
    },
    vat: {
        type: _db.Sequelize.DECIMAL(10, 2),
        defaultValue: 0
    },
    status: {
        type: _db.Sequelize.ENUM({
            values: ['pending', 'ready', 'expire']
        }),
        defaultValue: 'pending'
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
        fields: ['title'],
        unique: true
    }, {
        fields: ['price']
    }]
});

Products.sync({
    force: true
}).then(function () {
    // Table created

});