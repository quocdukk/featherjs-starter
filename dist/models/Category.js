'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Category = undefined;

var _db = require('../config/db.config');

var _Menu = require('./Menu');

var _sequelizePaginate = require('sequelize-paginate');

var Category = exports.Category = _db.sequelize.define('category', {
    id: {
        type: _db.Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: _db.Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
        fields: ['name'],
        unique: true
    }, {
        fields: ['id']
    }]
});
Category.belongsTo(_Menu.Menu);
(0, _sequelizePaginate.paginate)(Category);
Category.sync({
    force: true
}).then(function () {
    // Table created
    // let data = [{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    //     menuId:1
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    //     menuId:1
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 100) + 1)}`,
    // }]
    // return Category.bulkCreate(data)
});