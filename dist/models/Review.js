'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Review = undefined;

var _db = require('../config/db.config');

var _Product = require('./Product');

var Review = exports.Review = _db.sequelize.define('reviews', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: _db.Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: _Product.Product
        }
    },
    email: {
        type: _db.Sequelize.STRING,
        allowNull: false
    },
    fullname: {
        type: _db.Sequelize.STRING,
        allowNull: true
    },
    phone: {
        type: _db.Sequelize.STRING({ length: 10 }),
        allowNull: true
    },
    review: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    },
    value: {
        type: _db.Sequelize.INTEGER({ length: 5 }),
        allowNull: false
    }
}, {
    indexes: [{
        fields: ['id', 'email'],
        unique: true
    }]
});

Review.sync({
    force: true
}).then(function () {
    // Table created

});