'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Tag = undefined;

var _db = require('../config/db.config');

var _Category = require('./Category');

var _Product = require('./Product');

var Tag = exports.Tag = _db.sequelize.define('tags', {
    id: {
        type: _db.Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    category_id: {
        type: _db.Sequelize.INTEGER,
        references: {
            key: 'id',
            model: _Category.Category
        }
    },
    product_id: {
        type: _db.Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: _Product.Product
        }
    }
});

Tag.sync({
    force: true
}).then(function () {
    // Table created

});