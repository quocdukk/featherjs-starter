'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Tag = exports.Promotion = exports.Specification = exports.TypeProduct = exports.Type = exports.Comment = exports.Review = exports.Category = exports.Menu = exports.Product = undefined;

var _Product = require('./Product');

var _Menu = require('./Menu');

var _Category = require('./Category');

var _Review = require('./Review');

var _Comment = require('./Comment');

var _Type = require('./Type');

var _TypeProduct = require('./TypeProduct');

var _Specification = require('./Specification');

var _Promotion = require('./Promotion');

var _Tag = require('./Tag');

_Menu.Menu.hasMany(_Category.Category);

exports.Product = _Product.Product;
exports.Menu = _Menu.Menu;
exports.Category = _Category.Category;
exports.Review = _Review.Review;
exports.Comment = _Comment.Comment;
exports.Type = _Type.Type;
exports.TypeProduct = _TypeProduct.TypeProduct;
exports.Specification = _Specification.Specification;
exports.Promotion = _Promotion.Promotion;
exports.Tag = _Tag.Tag;