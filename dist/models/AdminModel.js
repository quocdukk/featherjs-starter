'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Admin = undefined;

var _db = require('../config/db.config');

var _hass = require('../config/hass.config');

var Admin = exports.Admin = _db.sequelize.define('admin', {
    username: {
        type: _db.Sequelize.STRING,
        allowNull: false
    },
    hash_password: {
        type: _db.Sequelize.STRING,
        allowNull: false
    }
});