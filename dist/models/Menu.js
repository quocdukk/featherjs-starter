'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Menu = undefined;

var _db = require('../config/db.config');

var _sequelizePaginate = require('sequelize-paginate');

var Menu = exports.Menu = _db.sequelize.define('menu', {
    id: {
        type: _db.Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: _db.Sequelize.STRING,
        allowNull: false
        // unique: true
    },
    description: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    indexes: [{
        fields: ['name'],
        unique: true
    }, {
        fields: ['id']
    }]
});
(0, _sequelizePaginate.paginate)(Menu);
Menu.sync({
    force: true
}).then(function () {
    // Table created
    // let data = [{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // },{
    //     name: `ABC ${Math.floor((Math.random() * 1000) + 1)}`,
    // }]
    // return Menu.bulkCreate(data)
});