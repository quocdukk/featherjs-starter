'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Specification = undefined;

var _db = require('../config/db.config');

var _Product = require('./Product');

var Specification = exports.Specification = _db.sequelize.define('specifications', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: _db.Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: _Product.Product
        }
    },
    content: {
        type: _db.Sequelize.TEXT,
        allowNull: true
    }
});

Specification.sync({
    force: true
}).then(function () {});