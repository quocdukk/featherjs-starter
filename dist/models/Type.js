'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Type = undefined;

var _db = require('../config/db.config');

var Type = exports.Type = _db.sequelize.define('types', {
    id: {
        type: _db.Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: _db.Sequelize.STRING({ length: 100 }),
        allowNull: false
    },
    active_flag: {
        type: _db.Sequelize.BOOLEAN,
        defaultValue: true
    }
});

Type.sync({
    force: true
}).then(function () {
    // Table created

});