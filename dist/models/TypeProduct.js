'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.TypeProduct = undefined;

var _db = require('../config/db.config');

var _Product = require('./Product');

var _Type = require('./Type');

var TypeProduct = exports.TypeProduct = _db.sequelize.define('type_products', {
    id: {
        type: _db.Sequelize.UUIDV4,
        primaryKey: true,
        autoIncrement: false
    },
    product_id: {
        type: _db.Sequelize.UUIDV4,
        references: {
            key: 'id',
            model: _Product.Product
        }
    },
    type_id: {
        type: _db.Sequelize.INTEGER,
        references: {
            key: 'id',
            model: _Type.Type
        }
    }
});

TypeProduct.sync({
    force: true
}).then(function () {
    // Table created

});